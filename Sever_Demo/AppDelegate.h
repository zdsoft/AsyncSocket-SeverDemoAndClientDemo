//
//  AppDelegate.h
//  Sever_Demo
//
//  Created by zc on 13-8-16.
//  Copyright (c) 2013年 zc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AsyncSocket.h"
@interface AppDelegate : NSObject <NSApplicationDelegate>
{
    AsyncSocket *listenSocket;
	NSMutableArray *connectedSockets;
	
	BOOL isRunning;

}
@property (assign) IBOutlet NSButton *button;
@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *portFiled;
- (IBAction)start:(id)sender;
@property (assign) IBOutlet id LogView;
@property (assign) IBOutlet NSTextField *LB;

@end
