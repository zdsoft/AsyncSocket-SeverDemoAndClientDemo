//
//  ViewController.m
//  ClientDemo
//
//  Created by zc on 13-8-16.
//  Copyright (c) 2013年 zc. All rights reserved.
//

#import "ViewController.h"
#define  host_IP @"192.168.112.102"
#define host_PORT 1111
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self connectSever:host_IP port:host_PORT];
    NSLog(@"hello?");
}

-(int)connectSever:(NSString *)ip port:(int )port{
    if (_client==nil) {
        _client =[[AsyncSocket alloc]initWithDelegate:self];
        NSError *error=nil;
        if (![_client connectToHost:host_IP onPort:host_PORT error:&error]) {
            NSLog(@" error --- %@  %@",error,[error localizedDescription]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection failed to host" message:@"error" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"YES", nil];
            [alert show];
            return 2;
        }
        //
        else{
            
            NSLog(@"连接成功");
            return 1;
        }
        
    }
    else{
        [_client readDataWithTimeout:-1 tag:0];
        return 0;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)bgTaped:(id)sender {
    [_sendMSG resignFirstResponder];
}

- (IBAction)ClientToSever:(id)sender {
    int state =[self connectSever:host_IP port:host_PORT];
    switch (state) {
        case 1:
            [self showMessage:@"连接成功"];
            break;
        case 2:
            [self showMessage:@"连接失败"];
            break;
        case 0:
            [self showMessage:@"不必再连，已连接"];
            break;
        default:
            break;
    }
}

- (IBAction)send:(id)sender {
    NSString *str=self.sendMSG.text;
    NSString *content =[str stringByAppendingString:@"\r\n"];
    NSLog(@"str --->%@",content);
    NSData *data=[content dataUsingEncoding:NSISOLatin1StringEncoding];
    [_client writeData:data withTimeout:-1 tag:0];
    
}

- (void) showMessage:(NSString *) msg{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert!"
        message:msg
                                                                           delegate:nil
                                                                  cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil];
        [alert show];
//        [alert release];
 }


#pragma mark -- Asy ---Soc Delegate
-(void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port{
    [_client readDataWithTimeout:-1 tag:0];
    NSLog(@"Delegate...  连接上服务器了,监听中");
}
-(void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err{
    NSLog(@"Delegate...willDisconnectWithError ...%@",err);
}
-(void)onSocketDidDisconnect:(AsyncSocket *)sock{
    NSString *msg=@"sorry ,this connect is failure";
    [self showMessage:msg];
    _client=nil;
    NSLog(@"Delegate...连接失败");
    
}
-(void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
    NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Delegate...接收到的数据是%@",str);
    self.resiveMSG.text=str;
//    [str release];
    [_client readDataWithTimeout:-1 tag:0];
}

@end
