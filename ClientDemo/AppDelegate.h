//
//  AppDelegate.h
//  ClientDemo
//
//  Created by zc on 13-8-16.
//  Copyright (c) 2013年 zc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
